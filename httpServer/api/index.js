"use strict";
import * as DB from "../models";
import * as servers from "../app.json";
var io = require("socket.io-emitter")({
  host: servers.redisServer,
  port: 6379
});
setInterval(function() {
  io.to("room2").emit("ev1", "message");
}, 5000);
 

export const httpApi = function(app) {
  app.post("/getAllMessageOfUser", function(req, res) {
    console.log("reqbody", req.body);
    DB.getAllRoomsOfUser(req.body.userId, function(err, rooms) {
      console.log(rooms);
      res.send(rooms);
    });
  });

  app.post("/sentMessage", function(req, res) {
    roomId = req.body.roomId;
    msg = req.body.message;
    DB.insertMessageInRoom(roomId, msg, function() {
      //TODO sent other user

      io.to(roomId), ("newMessage", msg);
    });
  });

  app.post("/createRoom", function(req, res) {
  
      
      var  userArray = req.body.users;
      DB.createRoom(userArray, null, function() {
        res.status(200).end('ok');
        
    });
  });
};
