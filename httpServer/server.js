import { httpApi } from "./api";
import express from "express";
import bodyParser from "body-parser";

var app = express();
var server = app.listen(3020, () => {
  console.log(
    "Well done, now I am listening on ",
    server.address().port,
    "as HTTP server"
  );
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));



app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.post('/test',function (req,res){

  console.log(req.body)
  res.send('ok')
})

httpApi(app);
