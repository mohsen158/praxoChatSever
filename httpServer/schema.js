
import mongoose from 'mongoose'
import * as servers from 'app.json'
mongoose.connect("mongodb://"+servers.mongoServer, { useNewUrlParser: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
    console.log('connected')
});

/**
 * Schemas
 */
let userSchema = new mongoose.Schema({
    name: String,
    rooms: [{ type: Schema.Types.ObjectId, ref: 'Room' }]
});
let roomSchema=new mongoose.Schema({
    title: String,
    users: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    messages:[]
})
let messageSchema= new mongoose.Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    text:String,
    room:{ type: Schema.Types.ObjectId, ref: 'Room' },
    time:[],
    seen:Boolean,
    replyTo:{type:Schema.Types.ObjectId,ref:'Message'}
})
toySchema.pre('save', function (next) {
    if (!this.created) this.created = new Date;
    next();
  })
export const User = mongoose.model('User', userSchema);
var user = new User({ name: 'mohsen' ,
rooms:[]})
user.save()



var childSchema = new mongoose.Schema({ name: 'string' });

var parentSchema = new mongoose.Schema({
    // Array of subdocuments
    children: [childSchema],
    // Single nested subdocuments. Caveat: single nested subdocs only work
    // in mongoose >= 4.2.0
    
});
export const Parent = mongoose.model('Parent', parentSchema);
var test = new Parent({ name: 'mohsen' })
test.save()
