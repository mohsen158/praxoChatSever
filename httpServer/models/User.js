
import mongoose from 'mongoose'
const { Model, Schema } = mongoose
const ObjectId = mongoose.Types.ObjectId;
import { DateTime } from 'luxon'
import { Message } from './index.js'
// mongoose.connect('mongodb://localhost:27017/praxo', { useNewUrlParser: true });
// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'connection error:'));
// db.once('open', function () {
//     // we're connected!
//     console.log('connected')
// });

/**
 * Schemas
 */
export const userSchema = new mongoose.Schema({
    userId: String,
    name: String,
    rooms: [{ type: Schema.Types.ObjectId, ref: 'Room' }]
});
// let roomSchema=new mongoose.Schema({
//     title: String,
//     users: [{ type: Schema.Types.ObjectId, ref: 'User' }],
//     create:{type:Date},
//     messages:[{ type: Schema.Types.ObjectId, ref: 'Message' }]
// })
// let messageSchema= new mongoose.Schema({
//     user: { type: Schema.Types.ObjectId, ref: 'User' },
//     text:String,
//     room:{ type: Schema.Types.ObjectId, ref: 'Room' },
//     sent:Object,
//     seen:Boolean,
//     replyTo:{type:Schema.Types.ObjectId,ref:'Message'}
// })


userSchema.statics.findOneOrCreate = function findOneOrCreate(condition, callback) {
    const self = this
    self.findOne(condition, (err, result) => {

        return result ? callback(err, result) : self.create(condition, (err, result) => { return callback(err, result) })
    })
}

/**
 * mideleware
 * TODO timezone not dynamic 
 * in app use :DateTime.fromISO("2018-08-12T15:57:24.353+04:30")
 * to get date object 
//  */
// userSchema.pre('save', function (next) {
//     if (!this.create) this.create =DateTime.local().setZone('Asia/Tehran').toString();
//     next();
//   })
//  export const User = mongoose.model('User', userSchema);
// var user = new Message({ text: 'mohsen' })
// user.save()
