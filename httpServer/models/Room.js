
import mongoose from 'mongoose'
const { Model, Schema } = mongoose
import {DateTime} from 'luxon'
import { messageSchema } from "./Message";
// mongoose.connect('mongodb://localhost:27017/praxo', { useNewUrlParser: true });
// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'connection error:'));
// db.once('open', function () {
//     // we're connected!
//     console.log('connected')
// });

/**
 * Schemas
 */
// let userSchema = new mongoose.Schema({
//     name: String,
//     rooms: [{ type: Schema.Types.ObjectId, ref: 'Room' }]
// });
export const roomSchema=new mongoose.Schema({
    title: String,
    create:{type:Date},
    users:[{type:Schema.Types.ObjectId,ref:'User'}],
    messages:[messageSchema]
    
})
// let messageSchema= new mongoose.Schema({
//     user: { type: Schema.Types.ObjectId, ref: 'User' },
//     text:String,
//     room:{ type: Schema.Types.ObjectId, ref: 'Room' },
//     sent:Object,
//     seen:Boolean,
//     replyTo:{type:Schema.Types.ObjectId,ref:'Message'}
// })

// roomSchema.virtual('users', {
//     ref: 'User',
//     localField: '_id',
//     foreignField: 'rooms'
//   });
  

/**
 * mideleware
 * TODO timezone not dynamic 
 * in app use :DateTime.fromISO("2018-08-12T15:57:24.353+04:30")
 * to get date object 
 */
roomSchema.pre('save', function (next) {
    if (!this.create) this.create =DateTime.local().setZone('Asia/Tehran').toString();
    next();
  })
//  export const Room = mongoose.model('Room', roomSchema);
// var user = new Message({ text: 'mohsen' })
// user.save()

//METHODES

/**
 * push one messag in specifixc room 
 * @param roomId
 * @param messag
 * 
 */

roomSchema.statics.pushMessageInRoom = function(roomId,message, cb) {
    this.findById(roomId, function (err, room) {
        room.messages.push(message._id)
        room.save();
    });
   
    return true
  };



