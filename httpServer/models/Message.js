import mongoose from 'mongoose'
const { Model, Schema } = mongoose
import { DateTime } from 'luxon'


/**
 * Schemas
 */
// let userSchema = new mongoose.Schema({
//     name: String,
//     rooms: [{ type: Schema.Types.ObjectId, ref: 'Room' }]
// });
// let roomSchema=new mongoose.Schema({
//     title: String,
//     users: [{ type: Schema.Types.ObjectId, ref: 'User' }],
//     messages:[]
// })
export const messageSchema = new mongoose.Schema({
    user: { type: String },
    text: String,
    // room: { type: Schema.Types.ObjectId, ref: 'Room' },
    sent: Object,
    seen: Boolean,
    roomId:{type :String},
    replyTo: { type: String }
})



/**
 * mideleware
 * TODO timezone not dynamic 
 * in app use :DateTime.fromISO("2018-08-12T15:57:24.353+04:30")
 * to get date object 
 */
messageSchema.pre('save', function (next) {
   if (!this.sent) this.sent = DateTime.local().setZone('Asia/Tehran').toString();
    next();
})
//  export const Message = mongoose.model('Message', messageSchema);

