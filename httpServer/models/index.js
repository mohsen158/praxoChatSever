import mongoose from "mongoose";
import faker from "faker";
import * as servers from "../app.json";
const { Model, Schema } = mongoose;
/**
 * Schemas
 */
import { roomSchema } from "./Room";

import { userSchema } from "./User";

/**
 * connection to mongodb
 */
mongoose.connect(
  "mongodb://" + servers.mongoServer,
  { useNewUrlParser: true }
);
var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function() {
  // we're connected!
  console.log("connected to db");
});
/**
 * remove element from array
 */

function remove(array, element) {
  return array.filter(e => e !== element);
}
/**
 * create Models
 */
// export const Message = mongoose.model("Message", messageSchema);
export const User = mongoose.model("User", userSchema);
export const Room = mongoose.model("Room", roomSchema);

// User.findOne({name:'mohsen'}).populate('rooms').exec(function (err, user) {
//     // if (err) return handleError(err);
//     console.log('The author is %s', user.rooms[0]);
//     user.rooms[0].populate('users').exec(function(err,room){
//         console.log('sdfsdf:',room)
//     })
//     // prints "The author is Ian Fleming"
//   });

/**
 * add user in specific room
 * @param roomId
 * @param userId
 *
 */
export function addUserToRoom(roomId, userId, cb) {
  Room.findOne({ _id: roomId }, function(err, room) {
    User.findOne({ _id: userId }, function(err, user) {
      room.users.push(user._id);
      user.rooms.push(room._id);
      user.save(function(err) {
        if (!err) {
          room.save(function(err) {
            if (!err) {
              cb();
            }
          });
        }
      });
    });
  });
}
/**
 * remove one user from room
 * @param {user _id} userId
 * @param {room _id} roomId
 */
export function removeUserFromRoom(userId, roomId) {
  Room.findOne({ _id: roomId }, function(err, room) {
    User.findOne({ _id: userId }, function(err, user) {
      room.users.push(user._id);
      user.rooms.push(room._id);
      room.users = remove(room.users, userId);
      user.rooms = remove(user.rooms, roomId);
      user.save();
      room.save();
    });
  });
}
/**
 * insert message to room
 * @param {roomId} roomId
 * @param {message} message
 */
export function insertMessageInRoom(roomId, message, cb) {
  Room.findById(roomId, function(err, room) {
    room.messages.unshift(message);
    room.save();
    cb(room.messages[0]);
  });
}

// User.create({name:'mohsen'})
// Room.create({title:'firstRoom'})
// insertMessageInRoom('5b72a2462fa8ed34ae3aadde', msg)
// var q=Room.
//   findOne({ _id: '5b7050efc437ed6010a0efea' }).
//   populate({
//     path: 'users',
//     // Get friends of friends - populate the 'friends' array for every friend
//     populate: { path: 'rooms' }
//   });
//   q.exec(function(err,q)
// {

//  q.users[0].rooms=remove(  q.users[0].rooms,q._id)
//  q.users[0].save()
//    // console.log(q.users[0].rooms[0])
// })
/**
 * Get all messages of one room
 * @param {roomId}
 */
export function getAllMessageOfRoom(roomId, cb) {
  Room.findById(roomId, function(err, room) {
    // console.log(room.messages)
    // list=JSON.stringify(room.messages)
    cb(room.messages[0]);
  });
}

/**
 * Get all messages of on user throw all room
 * @param userId
 */
export function getAllMessageOfUser(userId, cb) {
  messagesObject = object();

  getAllRoomsOfUser(userId, function(rooms) {
    rooms.forEach(room => {});
  });
}

/**
 * Get all rooms of one user
 * @param {userId} 'sdfsdffsf'
 * @returns {array} array of rooms id
 */
export function getAllRoomsOfUser(userId, cb) {
  User.findOne({userId:userId})
    .populate({
      path: "rooms",
      populate: { path: "users" }
    })
    .exec(function(err, user) {
      cb(err, user);
    });
  // User.findById(userId).select({ 'rooms': 1 }).exec(function (err, rooms) {
  //     cb(rooms.rooms)
  // })
}

export function createRoom(userArray, roomInfo, cb) {
  Room.create({ title: faker.lorem.word() }).then(function(room) {
    userArray.forEach(element => {
      User.findOneOrCreate(element, function(err, user) {
        console.log("user:", user);
        console.log("room", room);
        addUserToRoom(room._id, user._id,function(){
            cb()
        });
      });
    });
  });
}

// getAllRoomsOfUser('5b72a1d68fe7c633ef11037a',function(rooms)
// {
// console.log(rooms   )
// })

// addUserToRoom('5b72a2462fa8ed34ae3aadde','5b72a1d68fe7c633ef11037a')
// getAllMessageOfRoom('5b72a2462fa8ed34ae3aadde', function (list) {
//     console.log(list)
// })
// console.log(getAllMessageOfRoom("5b7050efc437ed6010a0efea"))
