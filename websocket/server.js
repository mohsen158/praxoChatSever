const io = require("socket.io")(3300);
const redisAdapter = require("socket.io-redis");
import * as servers from './app.json'
import { ioEvent } from "./socket";
io.adapter(redisAdapter({ host: servers.redisServer, port: 6379 } ));
io.use(function(socket, next) {
  //socket.name="mohsen"
  // var handshakeData = socket.request._query;
  // console.log(handshakeData)
  // console.log("hand", handshakeData);
  // make sure the handshake data looks good as before
  // if error do this:
  // next(new Error('not authorized'));
  // else just call next
  next();
});

ioEvent(io);
