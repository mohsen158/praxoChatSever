"use strict";
import * as DB from "../models";

import { roomSchema } from "../models/Room";
import { DateTime } from "luxon";
// const io = require("socket.io")(3300);

// const redisAdapter = require("socket.io-redis");
// io.adapter(redisAdapter({ host: "localhost", port: 6379 }));

// io.use(function(socket, next) {
//   var handshakeData = socket.request;
//   console.log("hand", handshakeData);
//   // make sure the handshake data looks good as before
//   // if error do this:
//   // next(new Error('not authorized'));
//   // else just call next
//   next();
// });

export const ioEvent = function(io) {
  io.of("/chatroom").on("connection", socket => {
    console.log('innnnn')
    let userId = socket.request._query.userId;
    socket.local.userId = userId;
    // console.log("one socket is conected:", socket);
    socket.broadcast.emit("hello", "to all clients except sender");
    // socket.to('room42').emit('hello', "to all clients in 'room42' room except sender");
    // io.emit('hello', 'to all clients');
    socket.on("eventTest", function(id, msg) {
      console.log(id, msg);
      socket.nsp.to("5b795a8646f7ff08b298146d").emit("event", { sfd: "sfsd" });
    });
    /**
     * Associate this socket to all rooms wiche has included before
     *
     */
    DB.getAllRoomsOfUser(userId, function(err, user) {
    
      if (user) {
        let rooms = user.rooms;
        rooms.forEach(element => {
          socket.join(element._id, () => {
            let rooms = Object.keys(socket.rooms);
            console.log(rooms); // [ <socket.id>, 'room 237' ]
          });
        });
      } else {
        //TODO user not exist
      }
    });

    /**
     * Get message from socket
     */

    socket.on("message", function(data) {
      data = JSON.parse(data);
      let groupId = data.groupId;
      let message = data.message;
      //TODO authorise msg
      DB.insertMessageInRoom(
        groupId,
        {
          user: socket.local.userId,
          roomId: groupId,
          text: message,
          sent: DateTime.local()
            .setZone("Asia/Tehran")
            .toString()
        },
        function(msg) {
          console.log("message sent to all :", msg);
          socket.nsp.to(groupId).emit("chat", msg);
        }
      );

      // DB.insertMessageInRoom
      //TODO send data throw room in cb
    });

    // DB.getAllRoomsOfUser

    // io.of('/').adapter.remoteJoin(socket.id, 'room1', (err) => {
    //     if (err) { /* unknown id */ }
    //     console.log('one socket is in room')
    // });

    //io.to('room2').emit('e1','message fro222');

    io.of("/").adapter.allRooms((err, rooms) => {
      // console.log(rooms); // an array containing all rooms (accross every node)
    });
  });
};
